"""Pipeline triggers."""
from . import baseline_trigger
from . import brew_trigger

TRIGGERS = {
    'baseline': baseline_trigger,
    'brew': brew_trigger
}
