"""Generic functions and constants."""
import re
import subprocess

from cki_lib import cki_pipeline
from cki_lib import gitlab
from cki_lib.logger import get_logger

LOGGER = get_logger(__name__)


def get_commit_hash(repository, git_ref):
    """Return commit hash for the ref in question.

    The reference needs to include the specifier, e.g. use 'refs/heads/main'
    instead of just 'main'; to avoid collisions.

    Args:
        repository: Git repository URL.
        git_ref:    Git reference to get commit hash for.

    Returns:
        String representing the commit hash or None if an error occurred.
    """
    LOGGER.debug('Getting the last commit from %s@%s', git_ref, repository)
    try:
        lines = subprocess.check_output(
            ['git', 'ls-remote', repository],
            timeout=60
        ).decode('utf-8').split('\n')
    # pylint: disable=broad-except
    except Exception:
        LOGGER.exception('Unable to list remote %s', repository)
        return None
    matches = [line for line in lines if line.endswith(git_ref)]
    if not matches:
        LOGGER.warning('Ref %s not found in remote %s', git_ref, repository)
        return None
    return matches[0].split()[0]


def was_tested(project, cki_pipeline_branch, commit_hash, variable_filter):
    """Find out if a commit was already tested or not.

    Args:
        project: GitLab project which the pipelines are associated with.
        cki_pipeline_branch: Project's branch responsible for testing of the repo.
        commit_hash: current git commit hash we want to test
        variable_filter: only consider pipelines with the given variables

    Returns:
        True if the watched commit was already tested, False otherwise.
    """
    escaped_variable_filter = {key: re.escape(value) if value is not None else None
                               for key, value in variable_filter.items()}
    gl_pipeline = cki_pipeline.last_pipeline_for_branch(project, cki_pipeline_branch,
                                                        variable_filter=escaped_variable_filter)

    if not gl_pipeline:
        return False
    if gitlab.get_variables(gl_pipeline).get('watched_repo_commit_hash') != commit_hash:
        return False
    if len(gl_pipeline.jobs.list(per_page=1, all=False)) == 0:
        return False
    return True
