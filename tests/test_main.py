"""Tests for __main__."""
import argparse
import copy
import pathlib
import tempfile
import unittest
from unittest import mock

from cki_lib.misc import EnvVarNotSetError

from tests import fakes
import triggers.__main__ as main_module

ENV_WITH_TOKENS = {
    'GITLAB_URL': 'http://gitlab.test',
    'GITLAB_TOKENS': '{"http://gitlab.test": "GITLAB_PRIVATE_TOKEN"}',
    'GITLAB_PRIVATE_TOKEN': 'private-token'
}


class TestMain(unittest.TestCase):
    """Tests for triggers.__main__.main()."""

    @mock.patch.dict('os.environ', ENV_WITH_TOKENS)
    @mock.patch.dict('triggers.TRIGGERS', {'fake-trigger': fakes})
    @mock.patch('gitlab.Gitlab')
    @mock.patch('builtins.open', new_callable=mock.mock_open, read_data='{}')
    @mock.patch('argparse.ArgumentParser.parse_args')
    @mock.patch('cki_lib.cki_pipeline.trigger_multiple')
    def test_main_calls(self, mock_trigger, mock_argparse, mock_file,
                        mock_gitlab):
        """Verify main() calls all the functions it should."""
        mock_gitlab.return_value = fakes.FakeGitLab()
        mock_argparse.return_value = argparse.Namespace(
            trigger='fake-trigger',
            config=None,
            kickstart=False,
            variables={},
        )
        mock_trigger.return_value = 'something'

        main_module.main()

        mock_trigger.assert_called_once()

    @mock.patch.dict('os.environ', ENV_WITH_TOKENS)
    @mock.patch('gitlab.Gitlab')
    @mock.patch('builtins.open', new_callable=mock.mock_open, read_data='{}')
    @mock.patch('argparse.ArgumentParser.parse_args')
    @mock.patch('triggers.brew_trigger.poll_triggers')
    def test_poll_triggers(self, mock_triggers, mock_argparse, mock_file,
                           mock_gitlab):
        """Verify brew trigger polling is called."""
        mock_gitlab.return_value = fakes.FakeGitLab()
        mock_argparse.return_value = argparse.Namespace(
            trigger='brew',
            config=None,
            kickstart=False,
            variables={},
        )

        main_module.main()

        mock_triggers.assert_called_once()

    @mock.patch.dict('triggers.TRIGGERS', {'fake-trigger': fakes})
    @mock.patch('gitlab.Gitlab')
    @mock.patch('builtins.open', new_callable=mock.mock_open, read_data='{}')
    @mock.patch('argparse.ArgumentParser.parse_args')
    @mock.patch('cki_lib.cki_pipeline.trigger_multiple')
    def test_required_envvars(self, mock_trigger, mock_argparse, mock_file,
                              mock_gitlab):
        """Verify EnvVarNotSet is raised if a required variable is missing."""
        mock_gitlab.return_value = fakes.FakeGitLab()
        mock_argparse.return_value = argparse.Namespace(
            trigger='fake-trigger',
            config=None,
            kickstart=False
        )
        mock_trigger.return_value = 'something'

        variables = ENV_WITH_TOKENS
        for variable in variables:
            reduced = copy.deepcopy(variables)
            del reduced[variable]
            with self.assertRaises(EnvVarNotSetError):
                main_module.main()

    @mock.patch.dict('os.environ', ENV_WITH_TOKENS)
    @mock.patch.dict('triggers.TRIGGERS', {'fake-trigger': fakes})
    @mock.patch('sys.argv', ['prog', 'fake-trigger'])
    @mock.patch('triggers.__main__.load')
    @mock.patch('gitlab.Gitlab')
    @mock.patch('builtins.open', new_callable=mock.mock_open, read_data='{}')
    @mock.patch('cki_lib.cki_pipeline.trigger_multiple')
    def test_main_include_path(self, mock_trigger, mock_file,
                               mock_gitlab, mock_cki_lib_yaml_load):
        """Verify main() calls the cki_lib's yaml.load."""
        mock_gitlab.return_value = fakes.FakeGitLab()
        mock_trigger.return_value = 'something'

        main_module.main()

        mock_trigger.assert_called_once()
        mock_cki_lib_yaml_load.assert_called_once()
        _, kwargs = mock_cki_lib_yaml_load.call_args
        self.assertTrue(kwargs['resolve_references'])
        self.assertTrue(kwargs['resolve_includes'])
        self.assertEqual(kwargs['file_path'], 'fake-trigger.yaml')

    @mock.patch.dict('os.environ', ENV_WITH_TOKENS)
    @mock.patch('gitlab.Gitlab')
    @mock.patch('argparse.ArgumentParser.parse_args')
    @mock.patch('triggers.brew_trigger.poll_triggers')
    def test_yaml_include(self, mock_triggers, mock_argparse, mock_gitlab):
        """Verify yaml including and referencing works."""
        mock_gitlab.return_value = fakes.FakeGitLab()
        with tempfile.TemporaryDirectory() as cwd:
            include_filename = 'include.yaml'
            include_fullpath = pathlib.Path(cwd) / include_filename
            include_fullpath.write_text(".foo:\n"
                                        "  one: 'two'\n")
            config_fullpath = pathlib.Path(cwd) / 'config.yaml'
            config_fullpath.write_text(f".include: {include_filename}\n"
                                       "three: !reference ['.foo', 'one']")

            mock_argparse.return_value = argparse.Namespace(
                trigger='brew',
                config=config_fullpath,
                kickstart=False,
                variables={},
            )

            main_module.main()

        mock_triggers.assert_called_once()
        args, _ = mock_triggers.call_args
        self.assertEqual(args[1]['three'], 'two')
