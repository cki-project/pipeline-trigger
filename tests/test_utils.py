"""Tests for triggers.utils."""
import unittest
from unittest import mock

from tests import fakes
from triggers import utils


class TestCheckForTested(unittest.TestCase):
    """Tests for utils.was_tested()."""

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_was_tested_true(self, mock_variables):
        """Check was_tested returns True if the commit was already tested."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_2', 'token', {'ref': 'cki_2'})

        mock_variables.return_value = {'watch_url': 'git_url',
                                       'ref': 'sha',
                                       'watched_repo_commit_hash': 'sha',
                                       'watch_branch': 'git_branch'}

        self.assertTrue(utils.was_tested(project, 'cki_2', 'sha', {
            'watch_url': 'git_url', 'watch_branch': 'git_branch', 'package_name': None}))
        self.assertEqual(mock_variables.call_count, 2)  # found

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_was_tested_other_commit(self, mock_variables):
        """Check was_tested returns False if the pipeline didn't run."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})

        mock_variables.return_value = {'watch_url': 'git_url',
                                       'ref': 'sha',
                                       'watch_url_commit_hash': 'different-sha',
                                       'watch_branch': 'git_branch'}

        self.assertFalse(utils.was_tested(project, 'cki_branch', 'sha', {
            'watch_url': 'git_url', 'watch_branch': 'git_branch', 'package_name': None}))
        self.assertEqual(mock_variables.call_count, 2)  # short-circuited

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_was_tested_no_commit(self, mock_variables):
        """Check was_tested returns False if the pipeline didn't run."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})

        mock_variables.return_value = {'watch_url': 'git_url',
                                       'ref': 'sha',
                                       'watched_repo_commit_hash': 'sha',
                                       'watch_branch': 'different_git_branch'}

        self.assertFalse(utils.was_tested(project, 'cki_branch', 'sha', {
            'watch_url': 'git_url', 'watch_branch': 'git_branch', 'package_name': None}))
        self.assertEqual(mock_variables.call_count, 3)

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_was_tested_retriggered(self, mock_variables):
        """Check that was_tested skips retriggered pipelines."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})

        mock_variables.return_value = {'CKI_DEPLOYMENT_ENVIRONMENT': 'retrigger',
                                       'watch_url': 'git_url',
                                       'ref': 'sha',
                                       'watched_repo_commit_hash': 'sha',
                                       'watch_branch': 'git_branch'}

        self.assertFalse(utils.was_tested(project, 'cki_branch', 'sha', {
            'watch_url': 'git_url', 'watch_branch': 'git_branch', 'package_name': None}))
        self.assertEqual(mock_variables.call_count, 3)

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_was_tested_no_jobs(self, mock_variables):
        """Check was_tested returns True if the commit was already tested."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_2', 'token', {'ref': 'cki_2'})
        project.pipelines[0].jobs.clear()

        mock_variables.return_value = {'watch_url': 'git_url',
                                       'ref': 'sha',
                                       'watched_repo_commit_hash': 'sha',
                                       'watch_branch': 'git_branch'}

        self.assertFalse(utils.was_tested(project, 'cki_2', 'sha', {
            'watch_url': 'git_url', 'watch_branch': 'git_branch', 'package_name': None}))


class TestGetCommitHash(unittest.TestCase):
    """Tests for utils.get_commit_hash()."""

    @mock.patch('subprocess.check_output')
    def test_commit_hash(self, mock_check_output):
        """Verify get_commit_hash returns the right value from git output."""
        mock_check_output.return_value = 'hash\t\trefs/heads/main'.encode()
        self.assertEqual(utils.get_commit_hash('repo', 'refs/heads/main'),
                         'hash')
